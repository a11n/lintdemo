package de.ad.lintrules;

import com.android.tools.lint.checks.infrastructure.LintDetectorTest;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Issue;
import java.util.Collections;
import java.util.List;

public class HelloWorldDetectorTest extends LintDetectorTest {
  @Override protected Detector getDetector() {
    return new HelloWorldDetector();
  }

  @Override protected List<Issue> getIssues() {
    return Collections.singletonList(HelloWorldDetector.ISSUE);
  }

  public void test() throws Exception {
    assertEquals(
        "AndroidManifest.xml:5: Information: Unexpected title \"@string/app_name\". Should be \"Hello world.\". [HelloWorld]\n"
            + "      android:label=\"@string/app_name\"\n"
            + "      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + "0 errors, 1 warnings\n", lintProject(
        xml("AndroidManifest.xml",
            "" + "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\"\n"
                + "    package=\"test.pkg\">\n" + "\n"
                + "  <application android:allowBackup=\"true\"\n"
                + "      android:label=\"@string/app_name\"\n"
                + "      android:supportsRtl=\"true\"\n" + "      >\n" + "\n" + "  </application>\n"
                + "\n" + "</manifest>")));
  }
}
